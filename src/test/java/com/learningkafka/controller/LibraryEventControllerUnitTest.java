package com.learningkafka.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.learningkafka.domain.Book;
import com.learningkafka.domain.LibraryEvent;
import com.learningkafka.producer.LibraryEventProducer;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = LibraryController.class)
@AutoConfigureMockMvc
public class LibraryEventControllerUnitTest {

    static String LIBRARY_API = "/v1/library-event";

    @Autowired
    public MockMvc mockMvc;

    @MockBean
    LibraryEventProducer libraryEventProducer;

    @Test
    public void postLibraryEvent() throws Exception {
        // given
        Book book = Book.builder().bookId(456).bookAuthor("Robert Ludlum").bookName("A Identidade Bourne").build();

        LibraryEvent libraryEvent = LibraryEvent.builder().libraryEventId(null).book(book).build();
        String json = new ObjectMapper().writeValueAsString(libraryEvent);
        BDDMockito.doNothing().when(libraryEventProducer).sendLibraryEventApproach2(libraryEvent);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(LIBRARY_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json);

        // when
        mockMvc.perform(request)
                .andExpect(status().isCreated());
    }

    @Test
    public void postLibraryEventError() throws Exception {
        // given
        Book book = Book.builder().bookId(null).bookAuthor(null).bookName(null).build();
        LibraryEvent libraryEvent = LibraryEvent.builder().libraryEventId(null).book(book).build();
        String json = new ObjectMapper().writeValueAsString(libraryEvent);
        BDDMockito.doNothing().when(libraryEventProducer).sendLibraryEventApproach2(libraryEvent);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(LIBRARY_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json);

        // when
        mockMvc.perform(request)
                .andExpect(status().is4xxClientError());
    }
}
