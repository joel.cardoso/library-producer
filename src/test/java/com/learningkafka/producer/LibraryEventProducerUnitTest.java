package com.learningkafka.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learningkafka.domain.Book;
import com.learningkafka.domain.LibraryEvent;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.concurrent.SettableListenableFuture;

import java.util.concurrent.ExecutionException;

@ExtendWith(MockitoExtension.class)
public class LibraryEventProducerUnitTest {

    @Mock
    KafkaTemplate<Integer, String> kafkaTemplate;

    @Spy
    ObjectMapper objectMapper = new ObjectMapper();

    @InjectMocks
    LibraryEventProducer eventProducer;

    @Test
    void sendLibraryEventApproach2Failure() throws JsonProcessingException, ExecutionException, InterruptedException {
        // given
        Book book = Book.builder().bookId(456).bookAuthor("Robert Ludlum").bookName("A Identidade Bourne").build();
        LibraryEvent libraryEvent = LibraryEvent.builder().libraryEventId(null).book(book).build();

        SettableListenableFuture listenableFuture = new SettableListenableFuture();
        listenableFuture.setException(new RuntimeException("Exception Calling Kafka"));
        BDDMockito.when(kafkaTemplate.send(BDDMockito.isA(ProducerRecord.class))).thenReturn(listenableFuture);

    }
}
